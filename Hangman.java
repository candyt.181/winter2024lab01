import java.util.Scanner;
public class Hangman{
	public static int isLetterInWord(String word, char c){
		for (int i = 0; i<4; i++){
			if (word.charAt(i) == c){
				return i;
			}
		}
		return -1;
	}
	public static char toUpperCase(char c){
		c = Character.toUpperCase(c);
		return c;
	}
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3){
		String theWord = "";
		
		if (letter0 == true){
			theWord = theWord + word.charAt(0);
		}
		else {
			theWord = theWord + "_";
		}
		
		if (letter1 == true){
			theWord = theWord + word.charAt(1);
		}
		else {
			theWord = theWord + "_";
		}
		
		if (letter2 == true){
			theWord = theWord + word.charAt(2);
		}
		else {
			theWord = theWord + "_";
		}
		
		if (letter3 == true){
			theWord = theWord + word.charAt(3);
		}
		else {
			theWord = theWord + "_";
		}
		System.out.println("Your result is " + theWord);
	}
	public static void runGame(String word){
		boolean letter0 = false;
		boolean letter1 = false;
		boolean letter2 = false;
		boolean letter3 = false;
		int numberOfGuesses = 0;
		
		while (numberOfGuesses < 6 && !(letter0 && letter1 && letter2 && letter3)){

			Scanner reader = new Scanner(System.in);
			char c = reader.nextLine().charAt(0);
			c = toUpperCase(c);
			

			if(isLetterInWord(word, c) == 0){
				letter0 = true; 
			}
			if(isLetterInWord(word, c) == 1){
				letter1 = true;
			}
			if(isLetterInWord(word, c) == 2){
				letter2 = true;
			}
			if(isLetterInWord(word, c) == 3){
				letter3 = true;
			}
			if(isLetterInWord(word, c) == -1){
				numberOfGuesses++;
			}
			printWork(word, letter0, letter1, letter2, letter3); 
		}
		if(numberOfGuesses>=6){
			System.out.println("Nice try ! But you did not successfully guess the word! :(");
		}
		if(letter0 && letter1 && letter2 && letter3){
			System.out.println("Congrats! You successfully guessed the word! :)");
		}
}


	public static void main(String[] args){
		Scanner bleh = new Scanner(System.in);
		System.out.println("Welcome to the Hangman Game! :D Please insert the word you want to be guessed.");
		String word = bleh.next();
		System.out.println("Please guess a letter of the word");
		word = word.toUpperCase();
		runGame(word); 
	}
}